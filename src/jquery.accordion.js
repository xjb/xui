/**
 * accordion - jQuery xui
 *
 * Licensed under the GPL:
 *   http://www.gnu.org/licenses/gpl.txt
 *
 * Copyright 2015 xjb [ beymy.en@gmail.com ]
 *
 */
(function($) {
	
	function initDom(target) {
		var state = $.data(target, 'accordion');
		var panels = $(target).addClass('accordion').children('div').addClass('accordion-body').wrap('<div class="accordion-panel"></div>');
		panels.each(function(i, domEle) {
			var jq =  $(domEle);
			var title = jq.attr('title');
			
			var header = $('<div class="accordion-header"></div>').html(title);
			var icon = $('<div class="accordion-icon"></div>');
			if(jq.is(':visible')) {
				icon.addClass('accordion-collapse')
			} else {
				icon.addClass('accordion-expand')
			}
			icon.appendTo(header);
			
			jq.before(header).removeAttr('title');
		});
	}
		
	function bindEvents(target) {
		var state = $.data(target, 'accordion');
		state.panels = $('>.accordion-panel', target); //取panel
		
		var t = $(target);
				
		$('>.accordion-header', state.panels).unbind('.accordion').bind('click.accordion', function(){
			var index = state.panels.index($(this).parent());
			toggle(target, index);
		});
	}
	
	function toggle(target, which) {
		var state = $.data(target, 'accordion');
		var panels = state.panels;
		
		var panel = panels[which]; //dom
		var h = $('>.accordion-header', panel);
		var b = $('>.accordion-body', panel);
		
		if(b.is(':visible')) {
			collapse(target, which);
		} else {
			expand(target, which);
		}
	}
	
	function expand(target, which) {
		var state = $.data(target, 'accordion');
		var panels = state.panels;
		var opts = state.options;
		
		var panel = panels[which]; //dom
		var h = $('>.accordion-header', panel);
		var b = $('>.accordion-body', panel);
		
		b.slideDown('fast');
		h.children('.accordion-icon').removeClass('accordion-expand').addClass('accordion-collapse');
		
		if(!opts.multiple) {
			state.panels.each(function(i) {
				if(i != which) {
					collapse(target, i);
				}
			});
		}
	}
	
	function collapse(target, which) {
		var state = $.data(target, 'accordion');
		var panels = state.panels;
		
		var panel = panels[which]; //dom
		var h = $('>.accordion-header', panel);
		var b = $('>.accordion-body', panel);
		
		b.slideUp('fast');
		h.children('.accordion-icon').removeClass('accordion-collapse').addClass('accordion-expand');
	}
	
	function expandAll(target, which) {
		var state = $.data(target, 'accordion');
		var panels = state.panels;
		var opts = state.options;
		
		if(opts.multiple) {
			panels.each(function(i) {
				expand(target, i);
			});
		}
	}
	
	function collapseAll(target, which) {
		var state = $.data(target, 'accordion');
		var panels = state.panels;
		
		panels.each(function(i) {
			collapse(target, i);
		});
	}
	
    $.fn.accordion = function(options, param) {
        if (typeof options == 'string') {
            return $.fn.accordion.methods[options](this, param);
        }
        options = options || {};
        return this.each(function() {
            var opts;
            var state = $.data(this, 'accordion');
            if (state) {
                opts = $.extend(state.options, options);
            } else {
                opts = $.extend({}, $.fn.accordion.defaults, $.fn.accordion.parseOptions(this), options);
                $.data(this, 'accordion', {
                    options: opts
                });
                
                if(opts.initDom) {
                	initDom(this);
                }
            }
            
            bindEvents(this);
        });
    };

    $.fn.accordion.methods = {
        options: function(jq) {
            return $.data(jq[0], 'accordion').options;
        },
        expand: function(jq, which) {
            return jq.each(function() {
                expand(this, which);
            });
        },
        collapse: function(jq, which) {
            return jq.each(function() {
                collapse(this, which);
            });
        },
        expandAll: function(jq) {
            return jq.each(function() {
                expandAll(this);
            });
        },
        collapseAll: function(jq) {
            return jq.each(function() {
                collapseAll(this);
            });
        }    
    };

    $.fn.accordion.parseOptions = function(target) {
        return $.extend({}, $.parser.parseOptions(target));
    };

    $.fn.accordion.defaults = {
    	initDom: false,
        multiple: true
    };

})(jQuery);