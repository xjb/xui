/**
 * messager - jQuery xui
 *
 * Licensed under the GPL:
 *   http://www.gnu.org/licenses/gpl.txt
 *
 * Copyright 2015 xjb [ beymy.en@gmail.com ]
 *
 * 依赖
 *	dialog
 */
(function($) {

    function show(dlg, type, speed, timeout) {
        if (!dlg) return;

        dlg.css('z-index', $.fn.dialog.defaults.zIndex++);
        switch (type) {
            case null:
                dlg.show();
                break;
            case 'slide':
                dlg.slideDown(speed);
                break;
            case 'fade':
                dlg.fadeIn(speed);
                break;
            case 'show':
                dlg.show(speed);
                break;
        }

        var timer = null;
        if (timeout > 0) {
            timer = setTimeout(function() {
                hide(dlg, type, speed);
            }, timeout);
        }
        dlg.hover(
            function() {
                if (timer) {
                    clearTimeout(timer);
                }
            },
            function() {
                if (timeout > 0) {
                    timer = setTimeout(function() {
                        hide(dlg, type, speed);
                    }, timeout);
                }
            }
        )
    }

    function hide(dlg, type, speed) {
        if (!dlg) return;

        switch (type) {
            case null:
                dlg.hide();
                break;
            case 'slide':
                dlg.slideUp(speed);
                break;
            case 'fade':
                dlg.fadeOut(speed);
                break;
            case 'show':
                dlg.hide(speed);
                break;
        }

        setTimeout(function() {
            dlg.remove();
        }, speed);
    }

    function buildDialog(title, content, buttons) {

        var dlg = $('<div class="dialog">' +
            '<div class="dialog-header">' +
            title +
            '<a class="dialog-close" href="javascript:void(0)"></a>' +
            '</div>' +
            '<div class="dialog-view">' +
            '<div class="dialog-body" style="padding: 10px;">' +
            content +
            '</div>' +
            '</div>' +
            '</div>').appendTo('body');


        if (buttons) {
            var tb = $('<div class="messager-button"></div>').appendTo($('>div.dialog-view>div.dialog-body', dlg));
            for (var text in buttons) {

                $('<a class="l-btn" href="javascript:void(0)">' +
                    '<span class="l-btn-left">' +
                    '<span class="l-btn-text">' +
                    text +
                    '</span>' +
                    '</span>' +
                    '</a>').appendTo(tb).bind('click', buttons[text]).linkbutton();
            }
        }

        return dlg;
    }

    $.messager = {
        show: function(options) {
            var opts = $.extend({
                showType: 'slide',
                showSpeed: 600,
                width: 250,
                height: 100,
                msg: '',
                title: '',
                timeout: 4000,
                style: {
                }
            }, options || {});
            	
            dlg = buildDialog(opts.title, opts.msg, null);
            dlg.dialog({
            	modal: false
            });
            show(dlg, opts.showType, opts.showSpeed, opts.timeout);
            $('>.dialog-view>.dialog-body', dlg)._outerWidth(opts.width-14)._outerHeight(opts.height-36);
            dlg.css(opts.style);
        },
        alert: function(title, msg, icon, fn) {
        	var dlg;
            var content = '<div class="messager-icon messager-' + icon + '"></div><div>' + msg + '</div>';

            content += '<div style="clear:both;"></div>';

            var buttons = {};
            buttons[$.messager.defaults.ok] = function() {
                dlg.dialog('close');
                if (fn) {
                    fn();
                    return false;
                }
            };
            dlg = buildDialog(title, content, buttons);
            $('>.dialog-view>.dialog-body', dlg)._outerWidth(250);
            dlg.dialog().dialog('open').dialog('center');
        },
        confirm: function(title, msg, fn) {
        	var dlg;
            var content = '<div class="messager-icon messager-question"></div>' + '<div>' + msg + '</div><div style="clear:both;"></div>';
            var buttons = {};
            buttons[$.messager.defaults.ok] = function() {
                dlg.dialog('close');
                if (fn) {
                    fn(true);
                    return false;
                }
            };
            buttons[$.messager.defaults.cancel] = function() {
                dlg.dialog('close');
                if (fn) {
                    fn(false);
                    return false;
                }
            };
            dlg = buildDialog(title, content, buttons);
            $('>.dialog-view>.dialog-body', dlg)._outerWidth(250);
            dlg.dialog().dialog('open').dialog('center');
        }
    };

    $.messager.defaults = {
        ok: '确认',
        cancel: '取消'
    };

})(jQuery);