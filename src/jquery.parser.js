/**
 * parser - jQuery xui
 *
 * Licensed under the GPL: http://www.gnu.org/licenses/gpl.txt
 *
 * Copyright 2015 xjb [ beymy.en@gmail.com ]
 *
 */
(function($) {
    $.parser = {
        /**
         * Example:
         * $.parser.parseOptions(target);
         * $.parser.parseOptions(target, ['id','title','width',{fit:'boolean',border:'boolean'},{min:'number'}]);
         */
        parseOptions: function(target, properties) {
            var t = $(target);
            var options = {};

            var s = $.trim(t.attr('data-options'));
            if (s) {
                if (s.substring(0, 1) == '[') { //对象数组支持
                    var arrayOptions = (new Function('return ' + s))();
                    $.each(arrayOptions, function(i, v) {
                        $.extend(options, v);
                    });
                } else {
                    if (s.substring(0, 1) != '{') {
                        s = '{' + s + '}';
                    }
                    options = (new Function('return ' + s))();
                }
            }

            if (properties) {
                var opts = {};
                for (var i = 0; i < properties.length; i++) {
                    var pp = properties[i];
                    if (typeof pp == 'string') {
                        if (pp == 'width' || pp == 'height' || pp == 'left' || pp == 'top') {
                            opts[pp] = parseInt(target.style[pp]) || undefined;
                        } else {
                            opts[pp] = t.attr(pp);
                        }
                    } else {
                        for (var name in pp) {
                            var type = pp[name];
                            if (type == 'boolean') {
                                opts[name] = t.attr(name) ? (t.attr(name) == 'true') : undefined;
                            } else if (type == 'number') {
                                opts[name] = t.attr(name) == '0' ? 0 : parseFloat(t.attr(name)) || undefined;
                            }
                        }
                    }
                }
                $.extend(options, opts);
            }
            return options;
        }
    };

    //设置box model宽度
    $.fn._outerWidth = function(width) {
        if (typeof width != 'number') {
            return this.outerWidth();
        }
        return this.each(function() {
            $(this).width(width - ($(this).outerWidth() - $(this).width()));
        });
    };

    //设置box model高度
    $.fn._outerHeight = function(height) {
        if (typeof height != 'number') {
            return this.outerHeight();
        }
        return this.each(function() {
            $(this).height(height - ($(this).outerHeight() - $(this).height()));
        });
    };
    
    $.fn._css = function(name) {
        return this[0].style[name];
    };
    
	$(window).resize(function() {
		$('body>.mask,body>.dialog').trigger('_resize');
	}).scroll(function() {
		$('body>.mask,body>.dialog').trigger('_scroll');
	});

})(jQuery);