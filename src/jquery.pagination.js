/**
 * pagination - jQuery xui
 *
 * Licensed under the GPL:
 *   http://www.gnu.org/licenses/gpl.txt
 *
 * Copyright 2015 xjb [ beymy.en@gmail.com ]
 *
 */
(function($) {
 
    function init(target) {
        var opts = $.data(target, 'pagination').options;
        var t = $(target).addClass('pagination');

        var pager = [];
        pager.push('<table border="0" cellspacing="0" cellpadding="0">');
        pager.push('<tbody>');
        pager.push('<tr>');
        if (opts.showPageList) {
            pager.push('<td><select class="pagination-page-list">');
            for (var i = 0; i < opts.pageList.length; i++) {
                if (opts.pageSize == opts.pageList[i]) {
                    pager.push('<option selected>' + opts.pageList[i] + '</option>');
                } else {
                    pager.push('<option>' + opts.pageList[i] + '</option>');
                }
            }
            pager.push('</select></td>');
            pager.push('<td><div class="pagination-btn-separator"></div></td>');
        }
        pager.push('<td><a class="l-btn l-btn-plain pagination-btn-first" href="javascript:void(0)">');
        pager.push('<span class="l-btn-left l-btn-icon-left">');
        pager.push('<span class="l-btn-text l-btn-empty">&nbsp;</span>');
        pager.push('<span class="l-btn-icon pagination-first">&nbsp;</span>');
        pager.push('</span>');
        pager.push('</a></td>');
        pager.push('<td><a class="l-btn l-btn-plain pagination-btn-prev" href="javascript:void(0)">');
        pager.push('<span class="l-btn-left l-btn-icon-left">');
        pager.push('<span class="l-btn-text l-btn-empty">&nbsp;</span>');
        pager.push('<span class="l-btn-icon pagination-prev">&nbsp;</span>');
        pager.push('</span>');
        pager.push('</a></td>');
        pager.push('<td><div class="pagination-btn-separator"></div></td>');
        pager.push('<td><span style="padding-left: 6px;">第</span></td>');
        pager.push('<td><input class="pagination-num" type="text" size="2" value="1"></td>');
        pager.push('<td><span class="pagination-after-page" style="padding-right: 6px;">共{pages}页</span></td>');
        pager.push('<td><div class="pagination-btn-separator"></div></td>');
        pager.push('<td><a class="l-btn l-btn-plain pagination-btn-next" href="javascript:void(0)">');
        pager.push('<span class="l-btn-left l-btn-icon-left">');
        pager.push('<span class="l-btn-text l-btn-empty">&nbsp;</span>');
        pager.push('<span class="l-btn-icon pagination-next">&nbsp;</span>');
        pager.push('</span>');
        pager.push('</a></td>');
        pager.push('<td><a class="l-btn l-btn-plain pagination-btn-last" href="javascript:void(0)">');
        pager.push('<span class="l-btn-left l-btn-icon-left">');
        pager.push('<span class="l-btn-text l-btn-empty">&nbsp;</span>');
        pager.push('<span class="l-btn-icon pagination-last">&nbsp;</span>');
        pager.push('</span>');
        pager.push('</a></td>');
        if (opts.showRefresh) {
            pager.push('<td><div class="pagination-btn-separator"></div></td>');
            pager.push('<td><a class="l-btn l-btn-plain pagination-btn-reload" href="javascript:void(0)">');
            pager.push('<span class="l-btn-left l-btn-icon-left">');
            pager.push('<span class="l-btn-text l-btn-empty">&nbsp;</span>');
            pager.push('<span class="l-btn-icon pagination-load">&nbsp;</span>');
            pager.push('</span>');
            pager.push('</a></td>');
        }
        pager.push('</tr>');
        pager.push('</tbody>');
        pager.push('</table>');
        pager.push('<div class="pagination-info">共{total}条记录</div>');
        pager.push('<div style="clear: both;"></div>');

        t.html(pager.join('')).find('.l-btn').linkbutton();;
    }

    function bindEvents(target) {
        var opts = $.data(target, 'pagination').options;
        var t = $(target);

        $('.pagination-btn-first', t).unbind('.pagination').bind('click.pagination', function() {
            if (opts.pageNumber > 1) {
                selectPage(target, 1);
            }
        });
        $('.pagination-btn-prev', t).unbind('.pagination').bind('click.pagination', function() {
            if (opts.pageNumber > 1) {
                selectPage(target, opts.pageNumber - 1);
            }
        });
        $('.pagination-btn-next', t).unbind('.pagination').bind('click.pagination', function() {
            var pageCount = Math.ceil(opts.total / opts.pageSize);
            if (opts.pageNumber < pageCount) {
                selectPage(target, opts.pageNumber + 1);
            }
        });
        $('.pagination-btn-last', t).unbind('.pagination').bind('click.pagination', function() {
            var pageCount = Math.ceil(opts.total / opts.pageSize);
            if (opts.pageNumber < pageCount) {
                selectPage(target, pageCount);
            }
        });
        $('.pagination-btn-reload', t).unbind('.pagination').bind('click.pagination', function() {        	
            selectPage(target, opts.pageNumber);
        });
        $('.pagination-num', t).unbind('.pagination').bind('keydown.pagination', function(e) {
            if (e.keyCode == 13) {
                var pageNumber = parseInt($(this).val()) || 1;
                selectPage(target, pageNumber);
            }
        });
        $('.pagination-page-list', t).unbind('.pagination').bind('change.pagination', function() {
            opts.pageSize = $(this).val();
            selectPage(target, opts.pageNumber);
        });
    }

    function selectPage(target, pageNumber) {
        var opts = $.data(target, 'pagination').options;
        var pageCount = Math.ceil(opts.total / opts.pageSize);
        if (pageNumber > pageCount) pageNumber = pageCount;
        if (pageNumber < 1) pageNumber = 1; //页码最小是1
        opts.onSelectPage.call(target, pageNumber, opts.pageSize);
        opts.pageNumber = pageNumber;
        showPagerInfo(target);
    }

    function showPagerInfo(target) {
        var opts = $.data(target, 'pagination').options;
        var t = $(target);
        var pageCount = Math.ceil(opts.total / opts.pageSize);

        if (pageCount < 1) { //总页码数最小是1
            pageCount = 1;
        }
        $('.pagination-num', t).val(opts.pageNumber);
        $('.pagination-after-page', t).text(opts.afterPageText.replace(/{pages}/, pageCount));
        $('.pagination-info', t).text(opts.displayMsg.replace(/{total}/, opts.total));

        $('.pagination-btn-first,.pagination-btn-prev', t).linkbutton((opts.pageNumber == 1) ? 'disable' : 'enable');
        $('.pagination-btn-next,.pagination-btn-last', t).linkbutton((opts.pageNumber == pageCount) ? 'disable' : 'enable');
    }
    
    function setLoading(target, loading) {
        var state = $.data(target, "pagination");
        var opts = state.options;
        var t = $(target);
        
        var icon = $('.pagination-btn-reload .l-btn-icon', t).removeClass('pagination-load pagination-loading');
        
        opts.loading = loading;
        
        icon.addClass(opts.loading ? 'pagination-loading' : 'pagination-load');
    };

    $.fn.pagination = function(options, param) {
        if (typeof options == 'string') {
            return $.fn.pagination.methods[options](this, param);
        }

        options = options || {};
        return this.each(function() {
            var state = $.data(this, 'pagination');
            var opts;
            if (state) {
                opts = $.extend(state.options, options);
            } else {
                opts = $.extend({}, $.fn.pagination.defaults, options);
                $.data(this, 'pagination', {
                    options: opts
                });
                init(this);
            }
            
            bindEvents(this);
            showPagerInfo(this);
        });
    };

    $.fn.pagination.methods = {
        options: function(jq) {
            return $.data(jq[0], 'pagination').options;
        },
        loading: function(jq) {
            return jq.each(function() {
                setLoading(this, true);
            });
        },
        loaded: function(jq) {
            return jq.each(function() {
                setLoading(this, false);
            });
        },
        select: function(jq, param) {
            return jq.each(function() {
                selectPage(this, param);
            });
        }
    };

    $.fn.pagination.parseOptions = function(target) {
        return $.parser.parseOptions(target);
    };

    $.fn.pagination.defaults = {
        total: 0,
        pageSize: 10,
        pageNumber: 1,
        pageList: [10, 20, 50, 100],
        showPageList: true,
        showRefresh: true,
        beforePageText: '第',
        afterPageText: '共{pages}页',
        displayMsg: '共{total}条记录',
        onSelectPage: function(pageNumber, pageSize) {},
        onChangePageSize: function(pageSize) {}
    };
})(jQuery);