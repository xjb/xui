/**
 * mask - jQuery xui
 *
 * Licensed under the GPL: http://www.gnu.org/licenses/gpl.txt
 *
 * Copyright 2015 xjb [ beymy.en@gmail.com ]
 *
 */
(function($) {
	
	function init(target) {
        var state = $.data(target, 'mask');
        var opts = state.options;
        var cc = $(target).css('zoom', '1'); //ie6 bug

        var mask = $('<div class="mask"></div>').css('display', 'block').appendTo(cc);
        var maskMsg = $('<div class="mask-msg"></div>').html(opts.msg).css('display', 'block').appendTo(cc);

        state.mask = mask;
        state.maskMsg = maskMsg;
    }
    
    function setSize(target) {
        var state = $.data(target, 'mask');
        if (state) {
            var opts = state.options;
            var mask = state.mask;
            var maskMsg = state.maskMsg;
            var cc = $(target);

            var css = {
                left: 0,
                top: 0,
                width: cc.outerWidth(),
                height: cc.outerHeight()
            };

            if (cc[0].tagName == 'BODY') {
                $.extend(css, getPageArea());
            }

            mask.css(css);
            maskMsg.css({
                left: (mask.outerWidth() - maskMsg.outerWidth()) / 2,
                top: (mask.outerHeight() - maskMsg.outerHeight()) / 2
            });
        }
    }

    function bindEvents(target) {
    	$(target).unbind('.mask').bind('_resize.mask', function() {
    		setSize(target);
    	});
    }

    function show(target) {
        var state = $.data(target, 'mask');
        var mask = state.mask;
        var maskMsg = state.maskMsg;
        $('select:not([disabled])', target).addClass('mask-disabled-select').attr('disabled', 'disabled'); //ie6下的select的z-index bug
        mask.css('display', 'block');
        maskMsg.css('display', 'block');
        setSize(target);
        state.showCount++;
    }

    function hide(target) {
        var state = $.data(target, 'mask');
        state.showCount--;
       	state.showCount = Math.max(state.showCount, 0); //最小为0
        if (state.showCount <= 0) {
            var mask = state.mask;
            var maskMsg = state.maskMsg;
            $('select.mask-disabled-select').removeAttr('disabled');
            mask.css('display', 'none');
            maskMsg.css('display', 'none');
        }
    }

    function destroy(target) {
        var state = $.data(target, 'mask');
        var mask = state.mask;
        var maskMsg = state.maskMsg;
        $('select.mask-disabled-select').removeClass('mask-disabled-select').removeAttr('disabled');
        mask.remove();
        maskMsg.remove();
        $(target).removeData('mask');
        $(target).unbind('.mask');
    }

    function getPageArea() {
        return {
            width: Math.max(document.body.clientWidth, document.documentElement.clientWidth),
            height: Math.max(document.body.clientHeight, document.documentElement.clientHeight)
        }
    }

    $.fn.mask = function(options, param) {
        if (typeof options == 'string') {
            return $.fn.mask.methods[options](this, param);
        }
        options = options || {};

        return this.each(function() {
            var state = $.data(this, 'mask');
            var opts;
            if (state) {
                opts = $.extend(state.options, options);
            } else {
                opts = $.extend({}, $.fn.mask.defaults, options);
                $.data(this, 'mask', {
                    showCount: 0,
                    options: opts
                });

                init(this);
            }

            if (opts.hide) {
                hide(this);
            } else {
                show(this);
            }

			setSize(this);
            bindEvents(this);
        });
    };

    $.fn.mask.methods = {
        show: function(jq) {
            jq.each(function() {
                show(this);
            });
        },
        hide: function(jq) {
            jq.each(function() {
                hide(this);
            });
        },
        destroy: function(jq) {
            jq.each(function() {
                destroy(this);
            });
        }
    }

    $.fn.mask.defaults = {
        msg: '处理中，请稍候',
        hide: false
    }
})(jQuery);
