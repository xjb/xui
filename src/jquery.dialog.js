/**
 * dialog - jQuery xui
 *
 * Licensed under the GPL:
 *   http://www.gnu.org/licenses/gpl.txt
 *
 * Copyright 2015 xjb [ beymy.en@gmail.com ]
 *
 */
(function($) {
	
	function initDom(target) {
		var state = $.data(target, 'dialog');
        var opts = state.options;
        var t = $(target).addClass('dialog-body');
        var dlg = $('<div class="dialog">'
        	+ '<div class="dialog-header"></div>'
        	+ '<div class="dialog-view"></div>'
        	+ '</div>');
        state.dialog = dlg;
        
        $('>.dialog-header', dlg).html(opts.title);
        if(opts.closable) {
        	$('>.dialog-header', dlg).append('<a class="dialog-close" href="javascript:void(0)"></a>');
        }
        t.after(dlg);
        $('>.dialog-view', dlg).append(t);
	}

    function init(target) {
        var state = $.data(target, 'dialog');
        var opts = state.options;
        var dialog = state.dialog;
        
        dialog.css({
            zIndex: opts.zIndex
        });

        state.mask = $('<div class="dialog-mask"></div>').appendTo('body');
        
        if(opts.toolbar) {
        	$(opts.toolbar).addClass('dialog-toolbar').prependTo($('>div.dialog-view', dialog));
        }
        
        if(opts.buttons) {
        	$(opts.buttons).addClass('dialog-button').appendTo($('>div.dialog-view', dialog));
        }

        $('>div.dialog-view>div.dialog-toolbar>a.l-btn', dialog).linkbutton();
        $('>div.dialog-view>div.dialog-button>a.l-btn', dialog).linkbutton();
    }

    function setSize(target) {
        var state = $.data(target, 'dialog');
        var opts = state.options;
        
        if(opts.modal) {
        	state.mask.css(getPageArea());
    	}
    }

    function bindEvents(target) {
    	var state = $.data(target, 'dialog');
        var opts = state.options;
        var dialog = state.dialog;

        $('>div.dialog-header>.dialog-close', dialog).unbind('.dialog').bind('click.dialog', function() {
            close(target);
        });

		if(opts.modal) {
	        dialog.unbind('.dialog').bind('_resize.dialog', function() {
	            if (dialog.is(':visible')) {
	                setSize(target);
	                center(target);
	            }
	        });
    	}
    }

    function open(target, param) {
        var state = $.data(target, 'dialog');
		var opts = state.options;
		var dialog = state.dialog;
		if(opts.onBeforeOpen.call(target, param) == false) {
			return;
		}
		setSize(target);
		if(opts.modal) {
        	state.mask.show();
    	}
        dialog.show();
        opts.onOpen.call(target, param);
    }

    function close(target) {
        var state = $.data(target, 'dialog');
        var opts = state.options;
        var dialog = state.dialog;
		if(opts.modal) {
        	state.mask.hide();
    	}
        dialog.hide();
        opts.onClose.call(target);
    }

    function center(target) {
        var state = $.data(target, 'dialog');
        var dialog = state.dialog;
        var position = {};
        var width = dialog.width();
        var height = dialog.height();
        position.left = Math.ceil(($(window).width() - width) / 2 + $(document).scrollLeft());
        position.top = Math.ceil(($(window).height() - height) / 2 + $(document).scrollTop());

        dialog.css({
            left: position.left,
            top: position.top
        });
    }

    function getPageArea() {
        return {
            //width: Math.max(document.body.scrollWidth, document.documentElement.scrollWidth),
            //height: Math.max(document.body.scrollHeight, document.documentElement.scrollHeight)
            //width: $(window).width() + $(document).scrollLeft(),
			//height: $(window).height() + $(document).scrollTop()
			
			width: Math.max(document.body.scrollWidth, document.documentElement.scrollWidth),
            height: Math.max(document.body.scrollHeight, document.documentElement.scrollHeight)
        }
    }

    $.fn.dialog = function(options, param) {
        if (typeof options == 'string') {
            if (typeof options == 'string') {
                return $.fn.dialog.methods[options](this, param);
            }
        }

        options = options || {};
        return this.each(function() {
            var state = $.data(this, 'dialog');
            var opts;
            if (state) {
                opts = $.extend(state.options, options);
            } else {
                opts = $.extend({}, $.fn.dialog.defaults, $.fn.dialog.parseOptions(this), options);
                state = $.data(this, 'dialog', {
                    options: opts
                });
                
                state.dialog = $(this);
				if(opts.initDom) {
					initDom(this);
				}
                init(this);
            }
            bindEvents(this);

            if (!opts.closed) {
                open(this);
            }
        });
    };


    $.fn.dialog.methods = {
        options: function(jq) {
            return $.data(jq[0], 'dialog').options;
        },
        open: function(jq, param) {
            return jq.each(function() {
                open(this, param);
            });
        },
        close: function(jq) {
            return jq.each(function() {
                close(this);
            });
        },
        center: function(jq) {
            return jq.each(function() {
                center(this);
            });
        }
    };

    $.fn.dialog.parseOptions = function(target) {
        return $.extend({}, $.parser.parseOptions(target,['title']));
    };

    $.fn.dialog.defaults = {
    	initDom: false,
    	title: '对话框',
    	toolbar: null,
    	buttons: null,
        zIndex: 1000,
        closable: true,
        closed: true,
        modal: true,
        onBeforeOpen: function() {
        	return true;
        },
        onOpen: function() {
        },
        onClose: function() {
        }
    };
})(jQuery);
