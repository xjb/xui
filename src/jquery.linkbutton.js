﻿
/**
 * linkbutton - jQuery xui
 *
 * Licensed under the GPL:
 *   http://www.gnu.org/licenses/gpl.txt
 *
 * Copyright 2015 xjb [ beymy.en@gmail.com ]
 *
 */
(function($) {
	
	function initDom(target) {
		var state = $.data(target, 'linkbutton');
        var opts = state.options;
        var t = $(target);
        
        opts.text = $.trim(t.html());
        
        t.empty().addClass('l-btn').removeClass('l-btn-plain');
		if (opts.plain){t.addClass('l-btn-plain')}
		
		var inner = $('<span class="l-btn-left"></span>').appendTo(t);
		if (opts.text){
			$('<span class="l-btn-text"></span>').html(opts.text).appendTo(inner);
		} else {
			$('<span class="l-btn-text l-btn-empty">&nbsp;</span>').appendTo(inner);
		}
		if (opts.iconCls){
			$('<span class="l-btn-icon">&nbsp;</span>').addClass(opts.iconCls).appendTo(inner);
			inner.addClass('l-btn-icon-left');
		}
	}

    function setDisabled(target, disabled) {
        var state = $.data(target, 'linkbutton');
        var opts = state.options;
        
        if (disabled) {
            opts.disabled = true;
            var href = $(target).attr('href');
            if (href) {
                state.href = href;
                $(target).attr('href', 'javascript:void(0)');
            }
            if (target.onclick) {
                state.onclick = target.onclick;
                target.onclick = null;
            }
			
			$(target).addClass('l-btn-disabled');
			if($(target).hasClass('l-btn-plain')) { //plain
				$(target).addClass('l-btn-plain-disabled');
			}
        } else {
            opts.disabled = false;
            if (state.href) {
                $(target).attr('href', state.href);
            }
            if (state.onclick) {
                target.onclick = state.onclick;
            }
			
			$(target).removeClass('l-btn-disabled l-btn-plain-disabled');
        }
    }

    $.fn.linkbutton = function(options, param) {
        if (typeof options == 'string') {
            return $.fn.linkbutton.methods[options](this, param);
        }

        options = options || {};
        return this.each(function() {
            var state = $.data(this, 'linkbutton');
            var opts;
            if (state) {
                opts = $.extend(state.options, options);
            } else {
            	opts = $.extend({}, $.fn.linkbutton.defaults, $.fn.linkbutton.parseOptions(this), options)
                $.data(this, 'linkbutton', {
                    options: opts
                });
                
                if(opts.initDom) {
                	initDom(this);
                }
            }
            
            setDisabled(this, opts.disabled);
        });
    };

    $.fn.linkbutton.methods = {
        options: function(jq) {
            return $.data(jq[0], 'linkbutton').options;
        },
        enable: function(jq) {
            return jq.each(function() {
                setDisabled(this, false);
            });
        },
        disable: function(jq) {
            return jq.each(function() {
                setDisabled(this, true);
            });
        }
    };

    $.fn.linkbutton.parseOptions = function(target) {
        return $.parser.parseOptions(target);
    };

    $.fn.linkbutton.defaults = {
    	initDom: false,
    	disabled: false,
    	plain: false,
    	text: '',
    	iconCls: null
    };

})(jQuery);
