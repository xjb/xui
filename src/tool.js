function showInfo(msg, callback) {
    $.messager.alert('提示', msg, 'info', callback);
}

function showError(msg, callback) {
    $.messager.alert('错误', msg, 'error', callback);
}

function showConfirm(msg, callback) {
    $.messager.confirm('确认', msg, callback);
}

function send(url, data, success, async) {
    $.ajax(url, {
        type: 'POST',
        async: (async == false ? false : true),
        contentType: 'application/json',
        dataType: 'json',
        data: $.toJSON(data),
        success: success
    });
}

function serializeForm(target) {
    var fm = $(target);
    var data = t.serializeObject();
    fm.each(function(i, e) {
        var opts = $.parser.parseOptions(e);
        if (opts.onSerialize) {
            opts.onSerialize.call(e, data);
        }
    });
    return data;
}

function search(target) {
    var opts = $.parser.parseOptions(target);
    var fm = $(target).closest('form');
    var url = opts.url || fm.attr('action');
    var params = serializeForm(fm);

    if (opts.onBefore && opts.onBefore.call(target, params) == false) {
        return false;
    }

    $(target).linkbutton('disable');
    $(opts.datagrid).datagrid('clearSelections').datagrid({
        url: url,
        pageNumber: 1,
        queryParams: params,
        onLoadSuccess: function(data) {
            $(target).linkbutton('enable');

            if (opts.onSuccess) {
                opts.onSuccess.call(target, data);
            }
        },
        onLoadError: function() {
            $(target).linkbutton('enable');
        }
    });
}

function openWin(page, param, title) {
    if (typeof param != 'string') {
        param = $.toJSON(param);
    }
    var url = page + '?' + encodeURIComponent(param);
    window.open(url, title, 'status=yes,toolbar=no,menubar=yes,location=no,resizable=yes,scrollbars=yes');
}

function exportData(target) {
    var opts = $.parser.parseOptions(target);
    var fm = $(target).closest('form');
    var url = opts.url;
    var params = serializeForm(fm);

    if (opts.onBefore && opts.onBefore.call(target, params) == false) {
        return false;
    }

    $(target).linkbutton('disable');
    send(url, params, function(data, textStatus, jqXHR) {
        $(target).linkbutton('enable');
        if (data.code == 0) {
            openWin('download.html', data.url, '下载' + data.url);
        }
    });
}

function submit(target, status) {
    var opts = $.parser.parseOptions(target);
    var fm = $(target).closest('form');
    var request = serializeForm(fm);

    if (opts.onBefore && opts.onBefore.call(target, request) == false) {
        return false;
    }

    if (fm.form('enableValidation').form('validate') != true) {
        if (opts.onValidateError) {
            opts.onValidateError.call(target, request);
        }
        return false;
    }

    fm.form('disableValidation');

    $(target).linkbutton('disable');
    send(opts.url, request, function(data) {
        $(target).linkbutton('enable');

        if (data.code == 0) {
            if (opts.onSuccess) {
                opts.onSuccess.call(target, request, data);
            }
        }
    });
}

function getKeys(keys, row) {
    var i, data = {};
    if (row) {
        for (i = 0; i < keys.length; i++) {
            data[keys[i]] = row[keys[i]];
        }
    }
    return data;
}

function batchSubmit(target) {
    var opts = $.parser.parseOptions(target);
    var dg = $(opts.datagrid);
    var dgOpts = dg.datagrid('options');
    var rows = dg.datagrid('getChecked');
    if (rows.length) {
        var keyRows = [];
        var i;
        for (i = 0; i < rows.length; i++) {
            keyRows.push(getKeys([dgOpts.idField], rows[i]));
        }
        var request = {
            rows: keyRows
        };

        if (opts.onBefore && opts.onBefore.call(target, request) == false) {
            return false;
        }

        if (confirm('确认要' + opts.msg + '选中的记录？')) {

            send(opts.url, request, function(data, textStatus, jqXHR) {
                if (data.code == 0) {
                    dg.datagrid('reload');
                    showInfo('操作成功');

                    if (opts.onSuccess) {
                        opts.onSuccess.call(target, request, data);
                    }
                }
            });
        }
    } else {
        showInfo('请选择记录！');
        return false;
    }
}

function formatField(rows, value, valueField, textField) {
    var i, len;
    valueField = valueField || 'value';
    textField = textField || 'text';
    if (rows) {
        len = rows.length;
        for (i = 0; i < len; i++) {
            if (rows[i][valueField] == value) {
                return rows[i][textField];
            }
        }
    }
    return null;
}

function sortTreeData(rows, options) {
    var treeDataMap, i, root, treeData;

    treeDataMap = {};
    for (i = 0; i < rows.length; i++) {
        treeDataMap[rows[i][options.id]] = rows[i];
    }

    //找root
    for (i = 0; i < rows.length; i++) {
        if (!treeDataMap[rows[i][options.pid]]) {
            root = rows[i];
            break;
        }
    }

    //root = treeDataMap[options.root];
    //treeDataMap[options.root] = null; //clear
    treeData = [];

    sortData(treeData, root);
    return treeData;

    function createNode(row) {
        var node = {
            id: row[options.id],
            text: row[options.text]
        };

        //图标处理
        if (options.icon && row[options.icon]) {
            node.iconCls = row[options.icon];
        }

        var i, name;
        if (options.attributes) {
            for (i = 0; i < options.attributes.length; i++) {
                name = options.attributes[i];
                if (row[name]) {
                    node.attributes = node.attributes || {};
                    node.attributes[name] = row[name];
                }
            }
        }

        return node;
    }

    function sortData(children, row) {
        while (row) {
            var pid = row[options.pid];
            var lid = row[options.lid];
            var rid = row[options.rid];

            var node = createNode(row, options)

            children.push(node);

            //children
            if (rid) {
                var child = treeDataMap[rid];
                if (child) {
                    node.children = [];
                    sortData(node.children, child);
                }
            }

            row = treeDataMap[lid];
        }
    }
}

function genTreeData(rows, options) {
    var treeDataMap = {};
    var treeData = [];
    var i, row, p, node;

    for (i = 0; i < rows.length; i++) {
        row = rows[i];
        treeDataMap[row[options.id]] = genEasyuiTreeNode(row, options);
    }

    //组装树    
    for (i = 0; i < rows.length; i++) {
        row = rows[i];
        node = treeDataMap[row[options.id]];
        p = treeDataMap[row[options.pid]];
        if (!p) { //没有找到parent
            treeData.push(node);
        } else { //找到parent
            p.children = p.children || [];
            p.children.push(node);
        }
    }

    return treeData;

    function genEasyuiTreeNode(row, options) {
        var node = {
            id: row[options.id],
            text: row[options.text]
        };

        if (options.icon && row[options.icon]) {
            node.iconCls = row[options.icon];
        }

        var i, name;
        if (options.attributes) {
            for (i = 0; i < options.attributes.length; i++) {
                name = options.attributes[i];
                if (row[name]) {
                    node.attributes = node.attributes || {};
                    node.attributes[name] = row[name];
                }
            }
        }

        return node;
    }
}

function hasNextStatus(rows, oper_in, proc_st, action) {
    var i, len;

    oper_in = oper_in || '0';
    proc_st = proc_st || '0';

    len = rows.length;
    for (i = 0; i < len; i++) {
        var row = rows[i];
        if (row.oper_in == oper_in && row.proc_st == proc_st && row.action == action) {
            return true;
        }
    }

    return false;
}

function parseQueryString() {
    var query = {};
    var queryString = window.location.search.substr(1);
    if (queryString.length > 0) {
        var pairs = queryString.split('&');
        for (var i = 0; i < pairs.length; i++) {
            var pair = pairs[i].split('=');
            if (pair.length < 2) {
                pair[1] = "";
            }
            query[decodeURIComponent(pair[0])] = decodeURIComponent(pair[1].replace(/\+/g, ' '));
        }
    }
    return query;
}

function inObjectArray(value, array, valueField) {
    var i;
    valueField = valueField || 'value';
    for (i = 0; i < array.length; i++) {
        if (array[i][valueField] == value) {
            return array[i];
        }
    }
    return null;
}

function transformStatus(form, status) {
	var fm = $(form);
	var target = fm[0];
    var opts = $.parser.parseOptions(target);

	fm.attr('data-status', status); //保存状态
    
    $('.btn', target).hide().filter('.' + status).show(); //按钮

	if ($.inArray(status, ['add', 'edit']) >= 0) {
		fm.form('enableEditable');
	} else {
		fm.form('disableEditable');
	}

    if (opts.onTransform) {
        opts.onTransform.call(target, status);
    }
}

function loadDetail(form, params, success) {
	var fm = $(form);
	var target = fm[0];
    var opts = $.parser.parseOptions(target, ['action']);
    
    fm.form('clear').attr('data-loaded', 'false');

    send(opts.action, params, function (data) {
        if (data.code == 0) {
            if (opts.onLoad) {
                opts.onLoad.call(target, data);
            }
            
            if(success) {
            	success(data);
            }
            
            fm.form('load', data.data).attr('data-loaded', 'true');
        }
    });
}

function view(target, status) {
    var opts = $.parser.parseOptions(target);
    var dgOpts = $(opts.datagrid).datagrid('options');
    var row = $(opts.datagrid).datagrid('getSelected');
    var fm = $(opts.form);
    
    status = status || 'view';
    if (row) {
        if (!dgOpts.idField) {
            showInfo('没有记录ID！');
            return false;
        }

        if (opts.onBefore && opts.onBefore.call(target, row) == false) {
            return false;
        }
        
        loadDetail(fm[0], getKeys([dgOpts.idField], row), opts.onSuccess);
		
    } else {
        showInfo('请选择记录！');
        return false;
    }
}

function add(target) {
    var opts = $.parser.parseOptions(target);
    var fm = $(opts.form);

    if (opts.onBefore && opts.onBefore.call(target) == false) {
        return false;
    }

    if (fm.attr('data-loaded') == 'true') {
        $.messager.confirm('确认', '确认清空当前数据', confirmCallback);
    } else {
        confirmCallback(true);
    }

    function confirmCallback(r) {
        if (r) {
            fm.form('clear').attr('data-loaded', 'false');
        }

        if (opts.onSuccess) {
            opts.onSuccess.call(target, r);
        }		
    }
}

function selectRow(rowIndex, rowData) {
    var t = $(this);
    var opts = t.datagrid('options');
    var tool = $('>.datagrid-body>.datagrid-toolbar .l-btn', t);
    var map = {};
    tool.each(function (i, domEle) {
    	var options = $.parser.parseOptions(domEle);
    	if (options.url) {
	        var url = options.url;
	        var action = url.substr(url.lastIndexOf('/') + 1);
	        if(inObjectArray(action, opts.transition, 'action') != null) {
	        	if(hasNextStatus(opts.transition, rowData['oper_in'], rowData['proc_st'], action)) {
                    $(domEle).linkbutton('enable').show();
                } else {
                	$(domEle).linkbutton('disable').hide();
                }
	        }
    	}
    });
}

function checkRow() {
	var t = $(this);
    var opts = t.datagrid('options');
    var tool = $('>.datagrid-body>.datagrid-toolbar .l-btn', t);
    var rows = $(this).datagrid('getChecked');

    tool.each(function (i, domEle) {
        var options = $.parser.parseOptions(domEle);
        if (options.url) {
            var url = options.url;
            var action = url.substr(url.lastIndexOf('/') + 1);

            if (action.indexOf('batch-') == 0) {
                action = action.substr(6);
                
                if(inObjectArray(action, opts.transition, 'action') != null) {
                	var len = rows.len;
                	var j, row, show = true;
		        	for (j = 0; result && j < len; j++) {
	                    row = rows[j];
	                    if (!hasNextStatus(opts.transition, row['oper_in'], row['proc_st'], action)) {
	                        show = false;
	                    }
	                }
	
	                if (len > 0 && show) {
	                    $(domEle).linkbutton('enable').show();
	                } else {
	                    $(domEle).linkbutton('disable').hide();
	                }
		        }
            }
        }
    });
}

$.ajaxSetup({
    complete: function (jqXHR, status) {
        if (jqXHR.status == 200) {
            var data = $.parseJSON(jqXHR.responseText || '{}');
            if (data.code == -11) {
                window.top.location.href = '/';
            } else if (data.code != 0) {
                if (data.msg != undefined) {
                    showError(data.msg);
                }
            }
        }
    }
});