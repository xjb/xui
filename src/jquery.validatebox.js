/**
 * validatebox - jQuery xui
 *
 * Licensed under the GPL: http://www.gnu.org/licenses/gpl.txt
 *
 * Copyright 2015 xjb [ beymy.en@gmail.com ]
 *
 */
(function($) {

    function init(target) {
        $(target).addClass('validatebox-text');
    }

    function destroy(target) {
        var state = $.data(target, 'validatebox');
        var opts = state.options;
        var tipTarget = opts.handler.tip(target);
        
        if (state.timer) {
            clearTimeout(state.timer);
        }
        
        tipTarget.tooltip('destroy');
        $(target).unbind('.validatebox');
        $(target).removeData('validatebox');
    }

    function bindEvents(target) {
        var state = $.data(target, 'validatebox');
        var opts = state.options;  
        var box = $(target);
        var tipTarget = opts.handler.tip(target);   

        if (state.timer) {
            clearTimeout(state.timer);
            state.timer = undefined;
        }
        box.unbind('.validatebox');
        if (opts.novalidate) {
            return;
        }
        box.bind('focus.validatebox', function() {
            state.validating = true;
            state.value = undefined;
            (function() {
                if (state.validating) {
                    if (state.value != box.val()) {
                        state.value = box.val();
                        if (state.timer) {
                            clearTimeout(state.timer);
                        }
                        state.timer = setTimeout(function() {
                            if(validate(target) == false) {
                            	showTip(target);
                            }
                        }, opts.delay);
                    }
                    setTimeout(arguments.callee, 200);
                }
            })();
        }).bind('blur.validatebox', function() {
            if (state.timer) {
                clearTimeout(state.timer);
                state.timer = undefined;
            }
            state.validating = false;
            
            //validate(this);
            hideTip(target);
        }).bind('mouseover.validatebox', function() {
            if (tipTarget.hasClass('validatebox-invalid')) {
                showTip(target);
            }
        }).bind('mouseout.validatebox', function() {
            if (!state.validating) {
                hideTip(target);
            }
        });
    }


    function showTip(target) {
        var state = $.data(target, 'validatebox');
        var opts = state.options;
        var tipTarget = opts.handler.tip(target);
        tipTarget.tooltip($.extend({}, opts.tipOptions, {
            content: state.message,
            position: opts.tipPosition,
            deltaX: opts.deltaX
        })).tooltip('show');
        state.tip = true;
    }

    function hideTip(target) {
        var state = $.data(target, 'validatebox');
        var opts = state.options;
        var tipTarget = opts.handler.tip(target);
        state.tip = false;
        tipTarget.tooltip('hide');
    }

    function validate(target) {
        var state = $.data(target, 'validatebox');
        var opts = state.options;
        var tip = state.tip;
        var box = $(target);        
        var value = box.val();
        var tipTarget = opts.handler.tip(target);

        function setTipMessage(msg) {
            $.data(target, 'validatebox').message = msg;
        }
        
        function validateRule(validType) {
        	var result = /([a-zA-Z_]+)(.*)/.exec(validType);
            var rule = opts.rules[result[1]];
            if (value && rule) {
                var param = eval(result[2]);
                if (!rule['validator'].call(target, value, param)) {
                    tipTarget.addClass('validatebox-invalid');

                    var message = rule['message'];
                    if (param) {
                        for (var i = 0; i < param.length; i++) {
                            message = message.replace(new RegExp('\\{' + i + '\\}', 'g'), param[i]);
                        }
                    }
                    setTipMessage(opts.invalidMessage || message);
                    return false;
                }
            }
            
            return true;
        }

        if (opts.novalidate || box.is(':disabled')) {
            return true;
        }

        if (opts.required) {
            if (value === '' || value === null) {
                tipTarget.addClass('validatebox-invalid');
                setTipMessage(opts.missingMessage);
                return false;
            }
        }
        if (opts.validType) {
        	if ($.isArray(opts.validType)) {
        		for (var i = 0; i < opts.validType.length; i++) {
                    if (!validateRule(opts.validType[i])) {
                        return false;
                    }
                }
        	} else {
        		if(!validateRule(opts.validType)) {
        			return false;
        		}
        	}
        }

        tipTarget.removeClass('validatebox-invalid');
        hideTip(target);
        
        return true;
    }

    function disableValidate(target, disable) {
        var opts = $.data(target, 'validatebox').options;
        var tipTarget = opts.handler.tip(target);
        if (disable != undefined) {
            opts.novalidate = disable;
        }
        if (opts.novalidate) {
            tipTarget.removeClass('validatebox-invalid');
            hideTip(target);
        }
        bindEvents(target);
    };

    $.fn.validatebox = function(options, param) {
        if (typeof options == 'string') {
            return $.fn.validatebox.methods[options](this, param);
        }

        options = options || {};
        return this.each(function() {
            var state = $.data(this, 'validatebox');
            if (state) {
                $.extend(state.options, options);
            } else {
                $.data(this, 'validatebox', {
                    options: $.extend({}, $.fn.validatebox.defaults, $.fn.validatebox.parseOptions(this), options)
                });
            }
			init(this);
            bindEvents(this);
        });
    };

    $.fn.validatebox.parseOptions = function(target) {
        return $.extend({}, $.parser.parseOptions(target));
    };

    $.fn.validatebox.methods = {
    	options: function(jq) {
            return $.data(jq[0], 'validatebox').options;
        },
        destroy: function(jq) {
            return jq.each(function() {
                destroy(this);
            });
        },
        validate: function(jq) {
            return jq.each(function() {
                validate(this);
            });
        },
        isValid: function(jq) {
            return validate(this[0]);
        },
        enableValidation: function(jq) {
            return jq.each(function() {
                disableValidate(this, false);
            });
        },
        disableValidation: function(jq) {
            return jq.each(function() {
                disableValidate(this, true);
            });
        }
    };

    $.fn.validatebox.defaults = {
        required: false,
        validType: null,
        delay: 200,
        missingMessage: '该输入项为必输项',
        invalidMessage: null,
        novalidate: false,
        tipPosition: 'right',
        deltaX: 5,
        tipOptions: {
        	showEvent: null,
        	hideEvent: null,
            onShow: function() {
                $(this).tooltip('tip').css({
                    color: '#000',
                    borderColor: '#CC9933',
                    backgroundColor: '#FFFFCC'
                });
            }
        },
        handler: {
            tip: function(target) {
            	//返回jquery对象
                return $(target);
            }
        },
        rules: {
            email: {
                validator: function(value) {
                    return /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i.test(value);
                },
                message: '请输入有效的电子邮件地址'
            },
            url: {
                validator: function(value) {
                    return /^(https?|ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(\#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(value);
                },
                message: '请输入有效的URL地址'
            },
            length: {
                validator: function(value, param) {
                    var len = $.trim(value).length;
                    return len >= param[0] && len <= param[1]
                },
                message: '输入内容长度必须介于{0}和{1}之间'
            }
        }
    };
})(jQuery);
