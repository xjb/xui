/**
 * select2 - jQuery xui
 *
 * Licensed under the GPL:
 *   http://www.gnu.org/licenses/gpl.txt
 *
 * Copyright 2015 xjb [ beymy.en@gmail.com ]
 *
 */
(function($) {
	
	function init(target) {
		var state = $.data(target, 'select2');
    	var opts = state.options;
    	$(target).addClass('select2').validatebox($.extend({
			required: opts.required,
			validType: opts.validType,
			novalidate: opts.novalidate
		},  opts.validateOptions));
    }
	
    function setDisabled(target, disabled) {
    	var state = $.data(target, 'select2');
        var opts = state.options;
        if (disabled) {
            opts.disabled = true;
            $(target).attr('disabled', 'disabled');
        } else {
            opts.disabled = false;
            $(target).removeAttr('disabled');
        }
    }

    //获取值
    function getValue(target) {
        return $(target).val();
    }

    //设置值
    function setValue(target, value) {
        $(target).val(value);
    }

    //加载数据
    function loadData(target, data) {
        var state = $.data(target, 'select2');
        var opts = state.options;
        state.data = opts.loadFilter.call(target, data);
        data = state.data || [];

        var dd = ['<option value="">--请选择--</option>'];
        for (var i = 0; i < data.length; i++) {
            var item = data[i];
            var v = item[opts.valueField];
            var s = item[opts.textField];

            dd.push('<option value="' + v + '">');
            dd.push(opts.formatter ? opts.formatter.call(target, item) : s);
            dd.push('</option>');
        }
        $(target).html(dd.join(''));
        //target.innerHTML = dd.join(''); //ie7 bug
        opts.onLoadSuccess.call(target, data);
    }

    //请求远程数据
    function request(target, url, param) {
        var opts = $.data(target, 'select2').options;
        if (url) {
            opts.url = url;
        }
        if (!opts.url)
            return;
        param = param || {};

        if (opts.onBeforeLoad.call(target, param) == false)
            return;

        $.ajax({
            type: opts.method,
            url: opts.url,
            data: $.toJSON(param),
            contentType: 'application/json',
            dataType: 'json',
            success: function(data) {
                loadData(target, data.rows);
            },
            error: function() {
                opts.onLoadError.apply(this, arguments);
            }
        });
    }

    $.fn.select2 = function(options, param) {
        if (typeof options == 'string') {
            return $.fn.select2.methods[options](this, param);
        }

        options = options || {};
        return this.each(function() {
            var state = $.data(this, 'select2');
            var opts;
            
            if (state) {
                opts = $.extend(state.options, options);
            } else {
                opts = $.extend({}, $.fn.select2.defaults, $.fn.select2.parseOptions(this), options);
                state = $.data(this, 'select2', {
                    options: opts,
                    data: []
                });
                init(this);
            }
            
            if (opts.data) {
                loadData(this, opts.data);
            }
            request(this);

            setDisabled(this, opts.disabled);
        });
    };


    $.fn.select2.methods = {
        options: function(jq) {
            return $.data(jq[0], 'select2').options;
        },
        getData: function(jq) {
            return $.data(jq[0], 'select2').data;
        },
        getValue: function(jq) {
            return getValue(jq[0]);
        },
        setValue: function(jq, value) {
            return jq.each(function() {
                setValue(this, [value]);
            });
        },
        disable: function(jq) {
            return jq.each(function() {
                setDisabled(this, true);
            });
        },
        enable: function(jq) {
            return jq.each(function() {
                setDisabled(this, false);
            });
        },
        clear: function(jq) {
            return jq.each(function() {
                $(this).html('');
            });
        },
        loadData: function(jq, data) {
            return jq.each(function() {
                loadData(this, data);
            });
        },
        reload: function(jq, url) {
            return jq.each(function() {
                request(this, url);
            });
        }
    };

    $.fn.select2.parseOptions = function(target) {
        return $.extend({}, $.parser.parseOptions(target));
    };

    $.fn.select2.defaults = {
        valueField: 'value',
        textField: 'text',
        mode: 'local', // or 'remote'
        method: 'post',
        url: null,
        data: null,
        validateOptions: {},
        formatter: function(row) {
            var opts = $(this).select2('options');
            return row[opts.textField];
        },
        loadFilter: function(data) {
            return data;
        },
        onBeforeLoad: function(param) {},
        onLoadSuccess: function() {},
        onLoadError: function() {}
    };
})(jQuery);
